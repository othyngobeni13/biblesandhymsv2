import 'package:flutter/material.dart';
import 'package:web_scraper/web_scraper.dart';

import '../../widgets/Book_Information.dart';
import '../../widgets/Chapter_Container.dart';

class ReadScreen extends StatefulWidget {
  const ReadScreen({super.key});

  @override
  _ReadScreenState createState() => _ReadScreenState();
}

class _ReadScreenState extends State<ReadScreen> {
  double total = 1;

  // initialize WebScraper by passing base url of website
  final webScraper = WebScraper('https://www.bible.com');

  // Response of getElement is always List<Map<String, dynamic>>
  List<Map<String, dynamic>>? productNames;
  late List<Map<String, dynamic>> productDescriptions;

  void fetchProducts() async {
    // Loads web page and downloads into local state of library
    if (await webScraper.loadWebPage('/bible/184/GEN.1.TSO89')) {
      setState(() {
        // getElement takes the address of html tag/element and attributes you want to scrap from website
        // it will return the attributes in the same order passed
        productNames = webScraper.getElement(
            'ul.list.ma0.pa0.bg-white.pb5.min-vh-100 > li.pa2.yv-gray50.hover-yv-blue10.f7.f6-m pl3',
            ['option']);
        productDescriptions = webScraper.getElement(
            'div.thumbnail > div.caption > p.description', ['class']);
      });
    }

    print('data:' '$productNames');
  }

  @override
  void initState() {
    super.initState();
    // Requesting to fetch before UI drawing starts
    fetchProducts();
  }

  late FixedExtentScrollController firstController;
  late FixedExtentScrollController secondController;
  int selected = 0;
  String book = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.brown[200],
        body: SingleChildScrollView(
          child: Align(
            alignment: Alignment.center,
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Container(
                        height: 300,
                        width: MediaQuery.of(context).size.width * 0.95,
                        decoration: BoxDecoration(
                          color: Colors.grey[50],
                          borderRadius: const BorderRadius.only(
                            bottomLeft: Radius.circular(80),
                            bottomRight: Radius.circular(80),
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                          ),
                        ),
                        child: Column(
                          children: const <Widget>[
                            SizedBox(height: 68),
                            BookInfo(
                              image: 'assets/bible 4.png',
                              title1: 'Mahungu',
                              title2: 'Lamanene',
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: Column(
                    children: const <Widget>[
                      SizedBox(
                        height: 20,
                      ),
                      ChapterCard(
                        title: 'Genesa',
                        maxChapter: 54,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      ChapterCard(
                        title: 'Eksoda',
                        maxChapter: 53,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
