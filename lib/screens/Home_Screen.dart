import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../widgets/Reading_Card_LIst.dart';
import 'chapter_screen/Tsonga_Chapter_Screen.dart';

class Homescreen extends StatelessWidget {
  const Homescreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.brown.shade200,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 60.0, left: 20.0, right: 20.0, bottom: 10.0),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    RichText(
                      text: TextSpan(
                        style: Theme.of(context).textTheme.headline4,
                        children: const [
                          TextSpan(text: 'What are you reading '),
                          TextSpan(
                            text: 'today?',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(6.0),
              child: Container(
                //height: MediaQuery.of(context).size.height * 0.65,
                width: MediaQuery.of(context).size.width * 0.95,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: const Offset(0, 3), // changes position of shadow
                    ),
                  ],
                  color: Colors.grey.shade100,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(40.0),
                      topRight: Radius.circular(40.0),
                      bottomLeft: Radius.circular(40.0),
                      bottomRight: Radius.circular(40.0)),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(
                      height: 35,
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Center(
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: SizedBox(
                                width: 100,
                                child: Text(
                                  'BIBLES',
                                  style: GoogleFonts.sacramento(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w900,
                                    color: Colors.brown,
                                  ),
                                ),
                              ),
                            ),
                            ReadingListCard(
                              image: 'assets/bible 5.jpeg',
                              title: 'Mahungu Lamanene',
                              langType: 'Tsonga Bible',
                              pressRead: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) {
                                    return ReadScreen();
                                  }),
                                );
                              },
                              pressDetails: () {},
                            ),
                            ReadingListCard(
                              image: 'assets/bible 4.png',
                              title: 'King James',
                              langType: 'English Bible',
                              pressRead: () {},
                            ),
                            ReadingListCard(
                              image: 'assets/bible 6.png',
                              title: 'King James',
                              langType: 'Venda Bible',
                              pressRead: () {},
                            ),
                          ],
                        ),
                      ),
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: SizedBox(
                              width: 100,
                              child: Text(
                                'HYMS',
                                style: GoogleFonts.sacramento(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w900,
                                  color: Colors.brown,
                                ),
                              ),
                            ),
                          ),
                          ReadingListCard(
                            image: 'assets/book2.jpg',
                            title: 'Mahungu Lamanene',
                            langType: 'Tsonga Bible',
                            pressRead: () {},
                          ),
                          ReadingListCard(
                            image: 'assets/Phymn.png',
                            title: 'King James',
                            langType: 'English Bible',
                            pressRead: () {},
                          ),
                          ReadingListCard(
                            image: 'assets/bible 6.png',
                            title: 'King James',
                            langType: 'Venda Bible',
                            pressRead: () {},
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
