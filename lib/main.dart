import 'package:bibles_and_hyms/screens/Home_Screen.dart';
import 'package:bibles_and_hyms/widgets/Rounded_Button.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BIBLES & HYMS APP',
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
      ),
      home: WelcomeScreen(),
    );
  }
}

class WelcomeScreen extends StatefulWidget {
  //const WelcomeScreen({super.key});
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: RadialGradient(
            radius: 0.9,
            stops: const [0.4, 0.9],
            colors: [
              Colors.brown.shade100,
              Colors.brown.shade300,
            ],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'BIBLES & HYMNS',
              style: GoogleFonts.aBeeZee(
                fontWeight: FontWeight.bold,
                fontSize: 35.0,
                letterSpacing: 1.5,
                color: Colors.black54,
              ),
            ),
            const SizedBox(
              height: 10.0,
            ),
            RoundedButton(
              text: 'Start reading',
              fontSize: 20,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return Homescreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
