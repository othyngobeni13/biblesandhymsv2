import 'package:flutter/material.dart';

import 'Two_Sided_rounded_buttons.dart';

class ReadingListCard extends StatelessWidget {
  final String image;
  final String title;
  final String langType;
  final VoidCallback? pressDetails;
  final VoidCallback pressRead;

  const ReadingListCard({
    required this.image,
    required this.title,
    required this.langType,
    this.pressDetails,
    required this.pressRead,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 0, bottom: 40),
      height: 245,
      width: 202,
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 10,
            top: 10,
            left: 5,
            right: 5,
            child: Container(
              height: 221,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(29),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 33,
                    color: Colors.grey.shade300,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(25.0),
            child: Image.asset(
              image,
              // 262.801061007958 200 image,
              width: 95,
            ),
          ),
          Positioned(
            top: 35,
            right: 10,
            child: IconButton(
              icon: const Icon(
                Icons.download,
              ),
              onPressed: () {},
            ),
          ),
          Positioned(
            top: 160,
            child: SizedBox(
              height: 74,
              width: 196,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 0),
                    child: RichText(
                      text: TextSpan(
                        style: const TextStyle(
                          color: Colors.black54,
                        ),
                        children: [
                          TextSpan(
                            text: '$title\n',
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text: '$langType',
                            style: const TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const Spacer(),
                  Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: pressDetails,
                        child: Container(
                          width: 101,
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          alignment: Alignment.center,
                          child: const Text('Details'),
                        ),
                      ),
                      Expanded(
                        child: TwoSideRoundedButton(
                          text: 'read',
                          press: pressRead,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
