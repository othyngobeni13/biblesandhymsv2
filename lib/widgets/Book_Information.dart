import 'package:flutter/material.dart';

class BookInfo extends StatelessWidget {
  final String image;
  final String title1;
  final String title2;

  const BookInfo({
    required this.image,
    required this.title1,
    required this.title2,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  '$title1',
                  style: Theme.of(context).textTheme.displaySmall,
                  textScaleFactor: 0.7,
                ),
                Text(
                  '$title2',
                  style: Theme.of(context).textTheme.displaySmall?.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                  textScaleFactor: 0.7,
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 10),
          child: Image.asset(
            '$image',
            height: 160,
            width: 120,
          ),
        )
      ],
    );
  }
}
